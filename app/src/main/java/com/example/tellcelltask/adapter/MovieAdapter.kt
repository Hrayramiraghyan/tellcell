package com.example.tellcelltask

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.tellcelltask.di.GlideApp
import com.example.tellcelltask.models.Movie
import kotlinx.android.synthetic.main.item_movie.view.*

class MovieAdapter(
    items: List<Movie>,
    val listener: MovieInteractionListener
) : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    private val movies = mutableListOf<Movie>()

    init {
        movies.addAll(items)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_movie,
                parent,
                false
            )
        )
    }

    interface MovieInteractionListener {
        fun onMovieSelected(position: Int, movie: Movie)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        (holder).bindCategory(position)
    }

    override fun getItemCount(): Int {
        return movies.size; }

    fun getItem(position: Int): Movie = movies[position]

    inner class MovieViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bindCategory(position: Int) {

            val movie = getItem(position)
            view.tv_movie_title.text = movie.title
            view.tv_vote_average.text = "Vote Average ".plus(movie.vote_average.toString())
            view.tv_popularity.text = "Popularity ".plus(movie.popularity.toString())
            view.tv_vote_count.text = "Vote Count ".plus(movie.vote_count.toString())
            view.cl_container.setBackgroundResource(R.drawable.movie_gradient_blue_bg)
            view.setOnClickListener {
                listener.onMovieSelected(position, movie)
            }

        }

        fun ImageView.displayImage(url: String?) {
            GlideApp.with(context)
                .setDefaultRequestOptions(
                    RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .fitCenter()
                )
                .load(url)
                .into(this)
        }
    }
}