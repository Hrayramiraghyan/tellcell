package com.example.tellcelltask.repository.util

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}