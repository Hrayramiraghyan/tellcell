package com.example.tellcelltask.repository.impl

import com.example.tellcelltask.data.converters.implementation.MovieDetailConverter
import com.example.tellcelltask.data.converters.implementation.MoviesConverter
import com.example.tellcelltask.data.network.api.MovieApi
import com.example.tellcelltask.models.Movie
import com.example.tellcelltask.models.Movies
import com.example.tellcelltask.repository.MovieRepository
import com.example.tellcelltask.repository.util.Resource
import com.example.tellcelltask.repository.util.ResponseHandler

class MovieRepositoryImpl(
    val movieApi: MovieApi,
    val moviesConverter: MoviesConverter,
    val movieDetailConverter: MovieDetailConverter,
    val responseHandler: ResponseHandler
) : MovieRepository {

    override suspend fun getPopularMovies(page: Int): Resource<Movies> {
        return try {
            val popularMovies = movieApi.getPopularMovies(page)
            responseHandler.handleSuccess(moviesConverter.dtoToModel(popularMovies))
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }

    override suspend fun getPopularMovieDetail(movieId: Int): Resource<Movie> {
        return try {
            val popularMovies = movieApi.getPopularMovieDetail(movieId)
            responseHandler.handleSuccess(movieDetailConverter.dtoToModel(popularMovies))
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }
}