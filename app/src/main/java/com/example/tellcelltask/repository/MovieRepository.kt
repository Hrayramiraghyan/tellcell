package com.example.tellcelltask.repository

import com.example.tellcelltask.models.Movie
import com.example.tellcelltask.models.Movies
import com.example.tellcelltask.repository.util.Resource

interface MovieRepository {

    suspend fun getPopularMovies(page:Int): Resource<Movies>

    suspend fun getPopularMovieDetail(movieId:Int): Resource<Movie>
}