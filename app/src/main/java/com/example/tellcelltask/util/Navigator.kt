package com.example.tellcelltask.util

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.example.tellcelltask.R
import com.example.tellcelltask.models.Movie
import com.example.tellcelltask.presenter.MainActivity
import com.example.tellcelltask.presenter.fragments.DetailMovieFragment
import com.example.tellcelltask.presenter.fragments.MovieFragment

open class Navigator {

    fun navigateToProductDetails(activity: FragmentActivity, movie: Movie) {
        getNavHomeFragment(activity)
            .childFragmentManager
            .beginTransaction()
            .add(R.id.nav_main_container, DetailMovieFragment.newInstance(movie))
            .addToBackStack(null)
            .commit()
    }

    fun navigateToMain(activity: Fragment) {
        activity
            .childFragmentManager
            .beginTransaction()
            .add(R.id.nav_main_container, MovieFragment.newInstance())
            .commit()
    }

    fun getNavHomeFragment(activity: FragmentActivity) =
        activity.supportFragmentManager.findFragmentByTag(MainActivity.NAV_MAIN_TAG) as Fragment

}