package com.example.tellcelltask.util

object Constants {
    const val API_KEY: String = "ae2a3c17f22c6bab7aa6dc7208b9b7ac"
    const val PAGINATION_SCROLL_POSITION: Int = 25
    const val ARG_MOVIE: String = "movie"
}