package com.example.tellcelltask.di

import com.example.tellcelltask.presenter.viewmodel.MovieDetailViewModel
import com.example.tellcelltask.presenter.viewmodel.MovieViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MovieViewModel(get()) }
    viewModel { MovieDetailViewModel(get()) }
}