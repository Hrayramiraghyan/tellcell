package com.example.tellcelltask.di

import com.example.tellcelltask.BuildConfig
import com.example.tellcelltask.data.network.api.MovieApi
import com.example.tellcelltask.data.network.util.RequestInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single {
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }
    single { RequestInterceptor() }
    single { provideRetrofitClient(get(), MovieApi::class.java) }
    single { provideOkHttpClient(get(), get()) }
}

private fun <T> provideRetrofitClient(okHttpClient: OkHttpClient, clazz: Class<T>): T {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient)
        .build()
        .create(clazz)
}

private fun provideOkHttpClient(
    loggingInterceptor: HttpLoggingInterceptor,
    requestInterceptor: RequestInterceptor
): OkHttpClient {
    return OkHttpClient()
        .newBuilder()
        .addNetworkInterceptor(loggingInterceptor)
        .addInterceptor(requestInterceptor)
        .build()
}