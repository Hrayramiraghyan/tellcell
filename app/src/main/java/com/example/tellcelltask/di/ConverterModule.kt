package com.example.tellcelltask.di

import com.example.tellcelltask.data.converters.DtoUIModelConverterContainer
import com.example.tellcelltask.data.converters.implementation.MovieDetailConverter
import com.example.tellcelltask.data.converters.implementation.MoviesConverter
import com.example.tellcelltask.data.converters.implementation.ProductionCountryConverter
import org.koin.core.qualifier.named
import org.koin.dsl.module

val converterModule = module {

    single {
        DtoUIModelConverterContainer(
            get(named(NAME_MOVIES_CONVERTER)),
            get(named(NAME_MOVIE_DETAIL_CONVERTER)),
            get(named(NAME_COUNTRY_CONVERTER))
        )
    }

    single { MoviesConverter() }
    single { MovieDetailConverter(get()) }
    single { ProductionCountryConverter() }

}

private const val NAME_MOVIES_CONVERTER = "movies_converter"
private const val NAME_MOVIE_DETAIL_CONVERTER = "movie_converter"
private const val NAME_COUNTRY_CONVERTER = "country_converter"

