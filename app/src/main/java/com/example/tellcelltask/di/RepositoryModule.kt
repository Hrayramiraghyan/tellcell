package com.example.tellcelltask.di

import com.example.tellcelltask.repository.MovieRepository
import com.example.tellcelltask.repository.impl.MovieRepositoryImpl
import com.example.tellcelltask.repository.util.ResponseHandler
import org.koin.dsl.module

val repositoryModule = module {

    single { ResponseHandler() }
    single<MovieRepository> { MovieRepositoryImpl(get(), get(), get(), get()) }
}