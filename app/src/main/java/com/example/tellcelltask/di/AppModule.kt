package com.example.tellcelltask.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.koin.dsl.module

val appModule = module {
    factory { provideGson() }
}

private fun provideGson(): Gson {
    return GsonBuilder().create()
}