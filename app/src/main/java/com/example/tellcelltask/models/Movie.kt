package com.example.tellcelltask.models

import java.io.Serializable

data class Movie(
    val id: Int,
    val vote_count: Int,
    val poster_path: String?,
    val adult: Boolean,
    val title: String?,
    val vote_average: Double,
    val popularity: Double,
    val overview: String?,
    val budget: Int,
    val production_countries: List<ProductionCountry>,
    val genres: List<Genre>

) : Serializable





