package com.example.tellcelltask.models

data class Genre(
    val id: Int,
    val name: String
)