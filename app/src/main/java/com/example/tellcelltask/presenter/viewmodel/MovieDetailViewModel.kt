package com.example.tellcelltask.presenter.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tellcelltask.models.Movie
import com.example.tellcelltask.repository.MovieRepository
import com.example.tellcelltask.repository.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MovieDetailViewModel(private val movieRepository: MovieRepository) : ViewModel() {

    private val popularMovieDetail: MutableLiveData<Resource<Movie>> = MutableLiveData()

    fun getPopularMovieDetail(): LiveData<Resource<Movie>> {
        return popularMovieDetail
    }

    fun setSelectedMovieDetail(movie: Movie) {
        popularMovieDetail.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            val data = movieRepository.getPopularMovieDetail(movie.id)
            withContext(Dispatchers.Main) {
                popularMovieDetail.value = data
            }
        }
    }
}