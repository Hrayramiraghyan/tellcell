package com.example.tellcelltask.presenter.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tellcelltask.MovieAdapter
import com.example.tellcelltask.R
import com.example.tellcelltask.models.Movie
import com.example.tellcelltask.presenter.viewmodel.MovieViewModel
import com.example.tellcelltask.repository.util.Status
import com.example.tellcelltask.util.Constants
import com.example.tellcelltask.util.EndlessRecyclerViewScrollListener
import com.example.tellcelltask.util.Navigator
import kotlinx.android.synthetic.main.fragment_movie.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class MovieFragment : Fragment() {

    private var CURRENT_PAGE = 1
    private var list: ArrayList<Movie> = ArrayList()

    private val vmMovie: MovieViewModel by viewModel()
    private lateinit var scrollListener: EndlessRecyclerViewScrollListener

    private val listener: MovieAdapter.MovieInteractionListener = object :
        MovieAdapter.MovieInteractionListener {
        override fun onMovieSelected(position: Int, movie: Movie) {
            Navigator().navigateToProductDetails(requireActivity(), movie)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vmMovie.getPopularMovies().observe(viewLifecycleOwner, Observer {

            when (it.status) {

                Status.LOADING -> {
                    pb_loading.visibility = View.VISIBLE
                }

                Status.SUCCESS -> it.data?.let { movies ->
                    pb_loading.visibility = View.GONE
                    if (savedInstanceState == null) {
                        movies.results.forEach({
                            if (it.adult == false) {
                                list.add(it)
                                initMoviesList(list)
                            }
                        })
                        rv_movies.scrollToPosition(list.size - Constants.PAGINATION_SCROLL_POSITION)
                    }
                }

                Status.ERROR -> {
                    //handle error
                }
            }
        })
    }

    private fun initMoviesList(list: List<Movie>) {

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rv_movies.layoutManager = layoutManager
        rv_movies.adapter = MovieAdapter(list, listener)

        scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                CURRENT_PAGE++
                vmMovie.loadPopularMovies(CURRENT_PAGE)
            }
        }

        rv_movies.addOnScrollListener(scrollListener)

    }

    companion object {
        @JvmStatic
        fun newInstance() = MovieFragment()
    }
}