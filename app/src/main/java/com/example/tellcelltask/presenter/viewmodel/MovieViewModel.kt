package com.example.tellcelltask.presenter.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tellcelltask.models.Movies
import com.example.tellcelltask.repository.MovieRepository
import com.example.tellcelltask.repository.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MovieViewModel(private val movieRepository: MovieRepository) : ViewModel() {

    //this is is an observable data holder class. Unlike a regular observable,
    // LiveData is lifecycle-aware, meaning it respects the lifecycle of other app components,
    // such as activities, fragments, or services. This awareness ensures LiveData only updates app component observers
    // that are in an active lifecycle state.

    //Resource type returns an object with parameters (val status: Status, val data: T?, val message: String?)
    //It help us to reach to status of request,get data object and catch exception in message
    private val popularMovies: MutableLiveData<Resource<Movies>> = MutableLiveData()

    fun getPopularMovies(): LiveData<Resource<Movies>> {
        return popularMovies
    }

    init {
        loadPopularMovies(1)
    }


    // this request returns popular movies and set data to Resource type MutableLiveData
    fun loadPopularMovies(page: Int) {
        popularMovies.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            val data = movieRepository.getPopularMovies(page)
            withContext(Dispatchers.Main) {
                popularMovies.value = data
            }
        }
    }

}