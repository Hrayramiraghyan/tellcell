package com.example.tellcelltask.presenter.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.tellcelltask.R
import com.example.tellcelltask.models.Movie
import com.example.tellcelltask.presenter.viewmodel.MovieDetailViewModel
import com.example.tellcelltask.repository.util.Status
import com.example.tellcelltask.util.Constants
import kotlinx.android.synthetic.main.fragment_detail_movie.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailMovieFragment : Fragment() {

    private val vmMovieDetail: MovieDetailViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vmMovieDetail.getPopularMovieDetail().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    pb_loading.visibility = View.VISIBLE
                }
                Status.SUCCESS -> it.data?.let { movieDetail ->
                    pb_loading.visibility = View.GONE
                    updateViews(movieDetail)
                }
                Status.ERROR -> {

                }
            }
        })

        if (savedInstanceState == null) {
            initMovie()
        }

        initListeners()
    }

    private fun initMovie() {
        arguments?.let {
            if (it.containsKey(Constants.ARG_MOVIE)) {
                val movie = it.getSerializable(Constants.ARG_MOVIE) as Movie
                updateViews(movie)
                vmMovieDetail.setSelectedMovieDetail(movie)
            }
        }
    }

    private fun updateViews(movie: Movie) {
        tv_movie_title.text = movie.title
        tv_vote_average.text = "Vote Average - ".plus(movie.vote_average.toString())
        tv_popularity.text = "Popularity - ".plus(movie.popularity.toString())
        tv_vote_count.text = "Vote Count - ".plus(movie.vote_count.toString())

        tv_budget.text = "Budget - ".plus(movie.budget.toString()).plus("$")

        if (!movie.production_countries.isNullOrEmpty()) {
            tv_production_country.text =
                "Production Country - ".plus(movie.production_countries.get(0).name)
        } else {
            tv_production_country.text = ""
        }

        val list: ArrayList<String> = ArrayList()
        if (!movie.genres.isNullOrEmpty()) {
            movie.genres.forEach({
                list.add(it.name)
            })
            tv_genres.text = "Genres -".plus(buildString(list))

        } else {
            tv_genres.text = ""
        }

        tv_overview.text = movie.overview.toString()

    }

    private fun buildString(list: List<String>): StringBuilder {
        val sb = StringBuilder(list.size)
        for (i in list.indices) {
            sb.append(list[i])
            if (i < list.size - 1) {
                sb.append(",")
            }
        }

        return sb
    }

    private fun initListeners() {
        iv_back.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                activity?.onBackPressed()
            }
        })
    }

    companion object {
        @JvmStatic
        fun newInstance(movie: Movie) = DetailMovieFragment().apply {
            arguments = Bundle().apply {
                putSerializable(Constants.ARG_MOVIE, movie)
            }
        }
    }

}