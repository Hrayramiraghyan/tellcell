package com.example.tellcelltask.presenter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.tellcelltask.R
import com.example.tellcelltask.presenter.fragments.NavMainFragment

class MainActivity : AppCompatActivity() {

    private var containerFragments = mutableMapOf<String, Fragment>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            containerFragments.clear()

            containerFragments[NAV_MAIN_TAG] = NavMainFragment.newInstance()
            supportFragmentManager.beginTransaction()
                .add(
                    R.id.main_container,
                    containerFragments[NAV_MAIN_TAG]!!,
                    NAV_MAIN_TAG
                )
                .commit()
        } else{
            containerFragments.clear()

            containerFragments[NAV_MAIN_TAG] =
                supportFragmentManager.findFragmentByTag(NAV_MAIN_TAG)!!
        }
    }

    override fun onBackPressed() {
        val currentNavBaseFragment = containerFragments[NAV_MAIN_TAG]
        val currentFragment = currentNavBaseFragment

        if (currentFragment != null) {
            if (currentFragment.childFragmentManager.backStackEntryCount > 0) {
                currentFragment.childFragmentManager.popBackStack()
            } else {
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }

    companion object {
        const val NAV_MAIN_TAG = "navMain"
    }

}