package com.example.tellcelltask.data.converters.implementation

import com.example.tellcelltask.data.converters.contracts.DtoUIModelConverter
import com.example.tellcelltask.data.network.dto.MovieDto
import com.example.tellcelltask.models.Movie

typealias MovieDetailDtoUIModelConverter = DtoUIModelConverter<MovieDto, Movie>

class MovieDetailConverter(val productionCountryConverter: ProductionCountryConverter) :
    MovieDetailDtoUIModelConverter {

    override fun dtoToModel(dtoObject: MovieDto): Movie {
        return Movie(
            dtoObject.id,
            dtoObject.vote_count,
            dtoObject.poster_path,
            dtoObject.adult,
            dtoObject.title,
            dtoObject.vote_average,
            dtoObject.popularity,
            dtoObject.overview,
            dtoObject.budget,
            dtoObject.production_countries.map { productionCountryConverter.dtoToModel(it) },
            dtoObject.genres

        )
    }

    override fun modelToDto(model: Movie): MovieDto {
        return MovieDto(
            model.id,
            model.vote_count,
            model.poster_path,
            model.adult,
            model.title,
            model.vote_average,
            model.popularity,
            model.overview,
            model.budget,
            model.production_countries.map { productionCountryConverter.modelToDto(it) },
            model.genres
        )
    }


}