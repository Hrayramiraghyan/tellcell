package com.example.tellcelltask.data.network.dto

data class ProductionCountryDto(
    val iso_3166_1: String?,
    val name: String?
)