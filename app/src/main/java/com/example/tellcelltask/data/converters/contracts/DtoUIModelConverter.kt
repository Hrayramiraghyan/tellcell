package com.example.tellcelltask.data.converters.contracts

interface DtoUIModelConverter<T, R> {
    fun dtoToModel(dtoObject: T): R
    fun modelToDto(model: R): T
}