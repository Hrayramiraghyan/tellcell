package com.example.tellcelltask.data.network.dto

import com.example.tellcelltask.models.Movie

data class MoviesDto(
    val page: Int,
    val total_results: Int,
    val total_pages: Int,
    val results: List<Movie>
)