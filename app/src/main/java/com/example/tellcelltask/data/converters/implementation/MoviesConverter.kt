package com.example.tellcelltask.data.converters.implementation

import com.example.tellcelltask.data.network.dto.MoviesDto
import com.example.tellcelltask.models.Movies
import com.example.tellcelltask.data.converters.contracts.DtoUIModelConverter

typealias MovieDtoUIModelConverter = DtoUIModelConverter<MoviesDto, Movies>

class MoviesConverter : MovieDtoUIModelConverter {

    override fun dtoToModel(dtoObject: MoviesDto): Movies {
        return Movies(
            dtoObject.page,
            dtoObject.total_results,
            dtoObject.total_pages,
            dtoObject.results
        )
    }

    override fun modelToDto(model: Movies): MoviesDto {
        return MoviesDto(
            model.page,
            model.total_results,
            model.total_pages,
            model.results
        )
    }


}