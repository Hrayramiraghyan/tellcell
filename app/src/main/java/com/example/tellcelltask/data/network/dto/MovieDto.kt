package com.example.tellcelltask.data.network.dto

import com.example.tellcelltask.models.Genre

data class MovieDto(
    val id: Int,
    val vote_count: Int,
    val poster_path: String?,
    val adult: Boolean,
    val title: String?,
    val vote_average: Double,
    val popularity: Double,
    val overview: String?,
    val budget: Int,
    val production_countries: List<ProductionCountryDto>,
    val genres: List<Genre>
)