package com.example.tellcelltask.data.network.util

import com.example.tellcelltask.util.Constants
import okhttp3.Interceptor
import okhttp3.Response

class RequestInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val request = chain.request().newBuilder()
        val originalHttpUrl = chain.request().url
        val url =
            originalHttpUrl.newBuilder().addQueryParameter("api_key", Constants.API_KEY).build()
        request.url(url)
        val response = chain.proceed(request.build())
        return response
    }

}