package com.example.tellcelltask.data.network.api

import com.example.tellcelltask.data.network.dto.MovieDto
import com.example.tellcelltask.data.network.dto.MoviesDto
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApi {

    @GET("movie/popular")
    suspend fun getPopularMovies(@Query("page") page: Int): MoviesDto

    @GET("movie/{movie_id}")
    suspend fun getPopularMovieDetail(@Path("movie_id") movieId: Int): MovieDto

}