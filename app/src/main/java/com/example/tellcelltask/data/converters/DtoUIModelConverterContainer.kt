package com.example.tellcelltask.data.converters

import com.example.tellcelltask.data.converters.implementation.MovieDetailConverter
import com.example.tellcelltask.data.converters.implementation.MoviesConverter
import com.example.tellcelltask.data.converters.implementation.ProductionCountryConverter


data class DtoUIModelConverterContainer(
    val movieConverter: MoviesConverter,
    val movieDetailConverter: MovieDetailConverter,
    val productionCountryConverter: ProductionCountryConverter
)