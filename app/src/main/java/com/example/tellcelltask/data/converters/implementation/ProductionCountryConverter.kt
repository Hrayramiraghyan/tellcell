package com.example.tellcelltask.data.converters.implementation

import com.example.tellcelltask.data.converters.contracts.DtoUIModelConverter
import com.example.tellcelltask.data.network.dto.ProductionCountryDto
import com.example.tellcelltask.models.ProductionCountry

typealias ProductionCountryDtoUIModelConverter = DtoUIModelConverter<ProductionCountryDto, ProductionCountry>

class ProductionCountryConverter : ProductionCountryDtoUIModelConverter {

    override fun dtoToModel(dtoObject: ProductionCountryDto): ProductionCountry {
        return ProductionCountry(
            dtoObject.iso_3166_1,
            dtoObject.name
        )
    }

    override fun modelToDto(model: ProductionCountry): ProductionCountryDto {
        return ProductionCountryDto(
            model.iso_3166_1,
            model.name
        )
    }
}


