package com.example.tellcelltask

import android.app.Application
import com.example.tellcelltask.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


//here i created modules of application i need and used Koin DI for injecting modules
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(
                listOf(
                    appModule,
                    repositoryModule,
                    viewModelModule,
                    networkModule,
                    converterModule
                )
            )
        }
    }
}